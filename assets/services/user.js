import request from '~/plugins/service'

const prefix = '/api/members'

// 获取用户信息
export const getUserInfo = (params) => {
    return request({
        url: `${prefix}/show.json`,
        methods: 'GET',
        data: params,
        params: params
    })
}