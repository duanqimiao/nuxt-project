import request from '~/plugins/service'

const prefix = '/api/nodes'

// 获取节点信息
export const getNodeInfo = (params) => {
    return request({
        url: `${prefix}/show.json`,
        methods: 'GET',
        data: params,
        params: params
    })
}