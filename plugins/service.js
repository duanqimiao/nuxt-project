import axios from "axios"

import { Message, Notification } from "element-ui"

axios.defaults.headers["X-Requested-With"] = "XMLHttpRequest"
axios.defaults.headers.post["Content-Type"] = "text/plain;charset=UTF-8"
let service = axios.create({
  // baseURL: "/",
  timeout: 10000
})

 // 请求拦截 可在请求头中加入token等
service.interceptors.request.use(config => {

  return config
}, error => {
  return Promise.reject(error)
})

// 响应拦截 对响应消息作初步的处理
service.interceptors.response.use(resp => {
  if (resp.data) {
    if (resp.status !== 200 ) {
      Message({
        type: "error",
        message: resp.data.message,
        duration: 5000
      })
    }
    return { code: resp.status, data: resp.data, msg: resp.message }
  } else {
    return resp
  }
}, error => {
  if (error.response) {
    switch (error.response.states) {
      case 400: {
        if (error.response && error.response.data && error.response.message) {
          Notification.error({
            title: "400错误",
            message: error.response.message,
            duration: 5000,
            closable: true
          })
        }
        break
      }
    }
  }
})

export default service