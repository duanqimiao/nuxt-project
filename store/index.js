import Vuex from 'vuex'

import user from './modules/user'

const createStore = () => {
    return new Vuex.Store({
        modules: {
            user
        },
        actions: {
            nuxtServerInit ({ commit }, { req }) {}
        }
    })
}

export default createStore