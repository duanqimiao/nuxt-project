/**
 * 用户模块
 */
import {USER_INFO} from '../types'

const state = {
    userInfo: {}
}

const getters = {
    userInfo(state) {
        return state.userInfo
    }
}

const actions = {
    update({commit}, value) {
        commit(USER_INFO, value)
    }
}

const mutations = {
    [USER_INFO](state, value) {
        state.userInfo = Object.assign(state.userInfo, value)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}